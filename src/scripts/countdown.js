/* eslint-disable complexity */
import { DateTime } from 'luxon';

const counter = () => {
  const checkLength = n => {
    if (n.length < 2) {
      return [0, n];
    } else {
      return n;
    }
  };

  const checkValidNumber = n => {
    if (n < 0) {
      return 0;
    } else {
      return n;
    }
  };

  const hoje = DateTime.local();
  const dataFinal = DateTime.local(2018, 5, 22, 18, 0);

  const dias = document.querySelector('.dias .counter__group');
  const horas = document.querySelector('.horas .counter__group');
  const minutos = document.querySelector('.minutos .counter__group');
  const diff = dataFinal.diff(hoje, ['hours', 'minutes', 'seconds']).values;

  const resultado = {
    dias: checkValidNumber(diff.hours)
      ? checkLength(diff.hours.toString(10).split(''))
      : '00',
    horas: checkValidNumber(diff.minutes)
      ? checkLength(diff.minutes.toString(10).split(''))
      : '00',
    minutos: checkValidNumber(diff.seconds)
      ? checkLength(
          Math.floor(diff.seconds)
            .toString(10)
            .split('')
        )
      : '00'
  };

  let diasFields = dias.querySelectorAll('.counter__item');
  const horasFields = horas.querySelectorAll('.counter__item');
  const minutosFields = minutos.querySelectorAll('.counter__item');

  if (resultado.dias.length > 2 && diasFields.length < 3) {
    const newChar = document.createElement('div');
    newChar.classList.add('counter__item');
    newChar.textContent = '-';
    dias.appendChild(newChar);
    diasFields = dias.querySelectorAll('.counter__item');
  } else if (resultado.dias.length < 3 && diasFields.length > 2) {
    dias.removeChild(diasFields[2]);
    diasFields = dias.querySelectorAll('.counter__item');
  }

  diasFields.forEach((field, index) => {
    field.textContent = resultado.dias[index];
  });

  horasFields.forEach((field, index) => {
    field.textContent = resultado.horas[index];
  });
  minutosFields.forEach((field, index) => {
    field.textContent = resultado.minutos[index];
  });
};

if (document.querySelector('.counter')) {
  counter();
}

const countdown = () => {
  window.setInterval(counter, 1000);
};

export default countdown;
