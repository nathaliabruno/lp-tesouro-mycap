const positionField = () => {
  const textGuide = document.querySelector('.textGuide');
  const campo = document.querySelector('.field');

  if (window.matchMedia('(min-width: 800px)').matches) {
    const getPosition = () => {
      const pos = textGuide.getBoundingClientRect().top + window.scrollY;
      campo.style.transform = `translateY(${pos - 10}px)`;
    };

    window.addEventListener('resize', () => {
      getPosition();
    });

    getPosition();
  }
};

export default positionField;
