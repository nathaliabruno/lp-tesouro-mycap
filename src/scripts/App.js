import validator from 'validator';
import maskInput from 'vanilla-text-mask';

import scrollIt from './scroller';


// Navegação
const nav = document.querySelectorAll('.link');
const types = document.querySelector('#types');
const compare = document.querySelector('#compare');

nav.forEach(el => {
  el.addEventListener('click', (e) => {
    e.preventDefault();
    if (el.class.contains('link__types')) scrollIt(types);
    if (el.class.contains('link__compare')) scrollIt(compare);
  });
});


// Acordeon
const openTheGate = el => {
  const abridor = el.querySelector('.faq__answer');
  const pega = () => {
    abridor.style.display = 'block';
    const altura = abridor.offsetHeight;
    abridor.style.display = 'none';
    return altura;
  };

  const altura = pega();

  abridor.style.display = 'block';
  abridor.style.height = '0';
  abridor.getBoundingClientRect();
  abridor.style.height = `${altura}px`;
};

const closeTheGate = el => {
  const abridor = el.querySelector('.faq__answer');

  abridor.style.height = '0';
  setTimeout(() => {
    abridor.style.display = 'none';
    abridor.style = '';
  }, 200);
};

// ===
const faqItens = document.querySelectorAll('.faq__item');
faqItens.forEach(item => {
  item.addEventListener('click', () => {
    if (item.classList.contains('faq--open')) {
      closeTheGate(item);
      item.classList.remove('faq--open');
    } else {
      openTheGate(item);
      item.classList.add('faq--open');
    }
  });
});

// Form submit
const formulario = document.querySelector('.contato__form form');

if (formulario) {
  const inputs = formulario.querySelectorAll('input');
  const textarea = formulario.querySelectorAll('textarea');

  formulario.addEventListener('submit', e => {
    inputs.forEach(field => {
      if (field.classList.contains('tel')) {
        if (!validator.isLength(field.value, 14)) {
          e.preventDefault();
          return field.classList.add('error');
        }
      }

      if (validator.isEmpty(field.value)) {
        e.preventDefault();
        return field.classList.add('error');
      } else {
        return field.classList.remove('error');
      }
    });

    textarea.forEach(field => {
      if (validator.isEmpty(field.value)) {
        e.preventDefault();
        return field.classList.add('error');
      } else {
        return field.classList.remove('error');
      }
    });

    return true;
  });

  const telMask = [
    '(',
    /[1-9]/,
    /\d/,
    ')',
    ' ',
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    '-',
    /\d/,
    /\d/,
    /\d/,
    /\d/
  ];
  const telInput = document.querySelector('.tel');
  maskInput({
    inputElement: telInput,
    mask: telMask,
    guide: false
  });
}
